# Pagina Web - Inventario

Integrantes:
- Felipe Carreño.
- Gerardo Muñoz.
- Benjamin Martin.

Profesores:
- Sebastian Ortega.
- Alex Rojas.



El presente Proyecto pertenece al modulo de Taller de Programacion Web, de la carrera Ingenieria Civil Bioinformatica en la universidad de Talca.

Se trata de un Sistema Web para controlar el inventario de una empresa que posea productos perecibles, en donde el usuario podra utilizar las herramientas disponibles en la Pagina Web para administrar de forma optima su inventario.

Se compone de 3 partes, la primera un codigo en HTML y la segunda, el estilo de la pagina web en formato CSS, ademas de una carpeta que contiene las imagenes utlizadas en el proyecto.

Cabe destacar que se ha implementado el framework Bootstrap para realizar un mejor trabajo y lograr que la pagina fuera Responsive.

Ademas de esto, cada uno de los 3 integrantes del grupo desarrollo su propia version de una pagina web para control de inventario.